<!DOCTYPE html>
<html lang="en">
<!-- Mirrored from code-wrapper.com/royal-homes/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 May 2023 09:26:16 GMT -->

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta name="description" content="" />
  <meta name="author" content="" />

  <!-- Favicon -->

  <title>Sebrango</title>

  <!-- Fonts  -->
  <link rel="preconnect" href="https://fonts.gstatic.com/" />
  <link href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@300;400;500;600;700&amp;display=swap"
    rel="stylesheet" />

  <!-- Style CSS -->
  <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
  <link rel="stylesheet" href="assets/css/owl.carousel.min.css" />
  <link rel="stylesheet" href="assets/css/owl.theme.default.min.css" />
  <link rel="stylesheet" href="assets/libs/fontawesome/css/all.css" />
  <link rel="stylesheet" href="assets/css/animate.min.css" />
  <link rel="stylesheet" href="assets/css/style.css" />




</head>


<?php
$ubicaciones = [];
$url = 'http://procesos.apinmo.com/xml/v2/Ru7VkGYC/10143-web.xml ';
$xml = file_get_contents($url);
$data = simplexml_load_string($xml);

foreach ($data->propiedad as $propiedad) {
  $ubicacion = (string) $propiedad->ciudad;
  array_push($ubicaciones, $ubicacion);







}







$ubicaciones_unicas = array_values(array_unique($ubicaciones));






?>




<body>
  <div class="main-wrapper">
    <!-- Banner Section Start -->
    <section id="home-banner" class="home-banner" style=" padding:0px;">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-12">
            <form class="banner-serach-form mt-5" id="searchForm" method="POST" action="">
              <ul class="nav nav-pills mb-3  justify-content-start" id="pills-tab" role="tablist"
                style="margin-left:20px;">
                <li class="nav-item" role="presentation">
                  <button class="nav-link active" id="pills-buy-tab" data-bs-toggle="pill" data-bs-target="#pills-buy"
                    type="button" role="tab" aria-controls="pills-buy" aria-selected="true">
                    Comprar
                  </button>
                </li>
                <li class="nav-item" role="presentation">
                  <button class="nav-link" id="pills-rent-tab" data-bs-toggle="pill" data-bs-target="#pills-rent"
                    type="button" role="tab" aria-controls="pills-rent" aria-selected="false">
                    Alquilar
                  </button>
                </li>

              </ul>
              <div class="tab-content banner-serach-form-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-buy" role="tabpanel" aria-labelledby="pills-buy-tab">
                  <div class="row">
                    <div class="col-lg-2 col-sm-12">
                      <input type="text" class="form-control mb-3 mb-lg-0 " placeholder="Numero Ref...."
                        id="comprar_ref" name="comprar_ref" />
                    </div>
                    <div class="col-lg-2 col-sm-6">
                      <select class="form-select form-select-md mb-3 mb-lg-0" aria-label=".form-select-md example"
                        name="tipo" id="tipo" placeholder="Tipo">
                        <option value="" selected>Tipo</option>
                        <option value="Piso">Piso</option>
                        <option value="Commercial">Casa</option>
                        <option value="Commercial">Chalet</option>
                        <option value="Commercial">Dúplex </option>
                        <option value="Commercial">Pareado </option>
                        <option value="Commercial">Ático </option>
                        <option value="Commercial">Apartamento </option>
                      </select>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                      <select class="form-select form-select-md mb-3 mb-md-0" aria-label=".form-select-md example"
                        name="ubicacion" id="ubicacion" placeholder="Ubicacion">
                        <option value="" selected>Ubicacion</option>
                        <?php
                        for ($i = 0; $i < count($ubicaciones_unicas); $i++) {
                          ?>
                          <option value="<?= $ubicaciones_unicas[$i] ?>"><?= $ubicaciones_unicas[$i] ?></option>
                          <?php
                        }
                        ?>

                      </select>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                      <a class="btn btn-outline-warning more-search-btn w-100 mb-3 mb-md-0" data-bs-toggle="collapse"
                        href="#more-option" role="button" aria-expanded="false" aria-controls="more-option">Más
                        <i class="fas fa-ellipsis-v ms-2"></i></a>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                      <a class="btn btn-outline-warning more-search-btn w-100 mb-3 mb-md-0" href="mapa.php">Mapa
                      </a>
                    </div>
                    <div class="col-lg-2 col-sm-6">

                      <button class="btn btn-warning property-search w-100 mb-3 mb-md-0" type="submit"
                        name="search">Buscar</button>
                    </div>
                  </div>
                </div>

                <div class="tab-pane fade" id="pills-rent" role="tabpanel" aria-labelledby="pills-rent-tab">
                  <div class="row">
                    <div class="col-lg-4 col-sm-12">
                      <input type="text" class="form-control mb-3 mb-lg-0" name="property-keyword"
                        placeholder="Numero Ref...." value="" />
                    </div>
                    <div class="col-lg-2 col-sm-6">
                      <select class="form-select form-select-md mb-3 mb-lg-0" aria-label=".form-select-md example"
                        name="property-type">
                        <option selected>Tipo</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                      </select>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                      <select class="form-select form-select-md mb-3 mb-lg-0" aria-label=".form-select-md example"
                        name="property-location">
                        <option selected>Ubicacion</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                      </select>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                      <a class="btn btn-outline-warning more-search-btn w-100 mb-3 mb-md-0" data-bs-toggle="collapse"
                        href="#more-option" role="button" aria-expanded="false" aria-controls="more-option">Mas
                        <i class="fas fa-ellipsis-v ms-2"></i></a>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                      <a href="javascript:void(0)" class="btn btn-warning property-search w-100 mb-3 mb-md-0"
                        type="submit">Buscar</a>

                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="pills-sale" role="tabpanel" aria-labelledby="pills-sale-tab">
                  <div class="row">
                    <div class="col-lg-4 col-sm-12">
                      <input type="text" class="form-control mb-3 mb-lg-0" name="property-keyword"
                        placeholder="Enter Keywords..." value="" />
                    </div>
                    <div class="col-lg-2 col-sm-6">
                      <select class="form-select form-select-md mb-3 mb-lg-0" aria-label=".form-select-md example"
                        name="property-type">
                        <option selected>Property Type</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                      </select>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                      <select class="form-select form-select-md mb-3 mb-lg-0" aria-label=".form-select-md example"
                        name="property-location">
                        <option selected>Location</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                      </select>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                      <a class="btn btn-outline-warning more-search-btn w-100 mb-3 mb-md-0" data-bs-toggle="collapse"
                        href="#more-option" role="button" aria-expanded="false" aria-controls="more-option">More
                        Options<i class="fas fa-ellipsis-v ms-2"></i></a>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                      <a href="javascript:void(0)" class="btn btn-warning property-search w-100 mb-3 mb-md-0">Search</a>
                    </div>
                  </div>
                </div>
              </div>

              <div class="collapse mt-3 position-absolute more-option " id="more-option">
                <div class="card border-0">
                  <div class="card-body">
                    <div class="row ">
                      <div class="col-md-3 col-sm-12 mb-3">
                        <select class="form-select form-select-md mb-3 mb-md-0" aria-label=".form-select-md example"
                          name="property-status">
                          <option selected>Tipo</option>
                          <option value="Any">Any</option>
                          <option value="For Rent">For Rent</option>
                          <option value="For Sale">For Sale</option>
                        </select>
                      </div>
                      <div class="col-md-3 col-sm-12 mb-3">
                        <select class="form-select form-select-md mb-3 mb-md-0" aria-label=".form-select-md example"
                          name="property-rooms">
                          <option selected>Habitaciones</option>
                          <option value="1">1 Rooms</option>
                          <option value="2">2 Rooms</option>
                          <option value="3">3 Rooms</option>
                          <option value="4">4 Rooms</option>
                          <option value="5">5 Rooms</option>
                        </select>
                      </div>
                      <div class="col-md-3 col-sm-12 mb-3">
                        <select class="form-select form-select-md mb-3 mb-md-0" aria-label=".form-select-md example"
                          name="property-bed">
                          <option selected>Dormitorios</option>
                          <option value="1">1 Bedrooms</option>
                          <option value="2">2 Bedrooms</option>
                          <option value="3">3 Bedrooms</option>
                          <option value="4">4 Bedrooms</option>
                          <option value="5">5 Bedrooms</option>
                        </select>
                      </div>
                      <div class="col-md-3 col-sm-12 mb-3">
                        <select class="form-select form-select-md mb-3 mb-md-0" aria-label=".form-select-md example"
                          name="property-bath">
                          <option selected>Baños</option>
                          <option value="1">1 Bathrooms</option>
                          <option value="2">2 Bathrooms</option>
                          <option value="3">3 Bathrooms</option>
                          <option value="4">4 Bathrooms</option>
                          <option value="5">5 Bathrooms</option>
                        </select>
                      </div>
                      <div class="col-md-3 col-sm-12 mb-3">
                        <select class="form-select form-select-md mb-3 mb-md-0" aria-label=".form-select-md example"
                          name="property-year">
                          <option selected>Antiguedad</option>
                          <option value="2017">2017</option>
                          <option value="2018">2018</option>
                          <option value="2019">2019</option>
                          <option value="2020">2020</option>
                          <option value="2021">2021</option>
                        </select>
                      </div>
                      <div class="col-md-3 col-sm-12 mb-3">
                        <select class="form-select form-select-md mb-3 mb-md-0" aria-label=".form-select-md example"
                          name="property-agent">
                          <option selected>Agentes</option>
                          <option value="Agent 1">Agente 1</option>
                          <option value="Agent 2">Agente 2</option>

                        </select>
                      </div>
                      <div class="col-md-3 col-sm-12 mb-3">
                        <input type="text" name="min-price" class="form-control mb-3 mb-md-0"
                          placeholder="Min Precio" />
                      </div>
                      <div class="col-md-3 col-sm-12 mb-3">
                        <input type="text" name="max-price" class="form-control mb-3 mb-md-0"
                          placeholder="Max Precio" />
                      </div>
                      <div class="col-md-3 col-sm-12 mb-3">
                        <input type="text" name="min-area" class="form-control mb-3 mb-md-0" placeholder="Min Area" />
                      </div>
                      <div class="col-md-3 col-sm-12 mb-3">
                        <input type="text" name="max-area" class="form-control mb-3 mb-md-0" placeholder="Max Area" />
                      </div>
                    </div>
                    <h3 class="text-center mb-3">Extras</h3>
                    <div class="row align-items-center">
                      <div class="col-xs-12 col-sm-4 col-md-3 p-2">
                        <div class="form-check">
                          <input id="Piscina" class="form-check-input" type="checkbox" name="amenities[]"
                            value="Piscina" />
                          <label for="Piscina" class="ms-2 form-check-label">Piscina</label>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-4 col-md-3 p-2">
                        <div class="form-check">
                          <input id="Parking" class="form-check-input" type="checkbox" name="amenities[]"
                            value="Parking" />
                          <label for="Parking" class="ms-2 form-check-label">Parking</label>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-4 col-md-3 p-2">
                        <div class="form-check">
                          <input id="Ascensor" class="form-check-input" type="checkbox" name="amenities[]"
                            value="Ascensor" />
                          <label for="Ascensor" class="ms-2 form-check-label">Ascensor</label>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-4 col-md-3 p-2">
                        <div class="form-check">
                          <input id="Terraza" class="form-check-input" type="checkbox" name="amenities[]"
                            value="Terraza" />
                          <label for="Terraza" class="ms-2 form-check-label">Terraza</label>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-4 col-md-3 p-2">
                        <div class="form-check">
                          <input id="Amueblado" class="form-check-input" type="checkbox" name="amenities[]"
                            value="Amueblado" />
                          <label for="Amueblado" class="ms-2 form-check-label">Amueblado</label>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-4 col-md-3 p-2">
                        <div class="form-check">
                          <input id="Urbanizacion" class="form-check-input" type="checkbox" name="amenities[]"
                            value="Urbanizacion" />
                          <label for="Urbanizacion" class="ms-2 form-check-label">Urbanizacion</label>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-4 col-md-3 p-2">
                        <div class="form-check">
                          <input id="Obra Nueva" class="form-check-input" type="checkbox" name="amenities[]"
                            value="Obra Nueva" />
                          <label for="Obra Nueva" class="ms-2 form-check-label">Obra Nueva</label>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-4 col-md-3 p-2">
                        <div class="form-check">
                          <input id="Vistasalmar" class="form-check-input" type="checkbox" name="amenities[]"
                            value="Vistasalmar" />
                          <label for="Vistasalmar" class="ms-2 form-check-label">Vistas al Mar</label>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-4 col-md-3 p-2">
                        <div class="form-check">
                          <input id="Fireplace" class="form-check-input" type="checkbox" name="amenities[]"
                            value="Fireplace" />
                          <label for="Fireplace" class="ms-2 form-check-label">Fireplace</label>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-4 col-md-3 p-2">
                        <div class="form-check">
                          <input id="Fire-Alarm" class="form-check-input" type="checkbox" name="amenities[]"
                            value="Fire Alarm" />
                          <label for="Fire-Alarm" class="ms-2 form-check-label">Fire Alarm</label>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-4 col-md-3 p-2">
                        <div class="form-check">
                          <input id="Emergency-Exit" class="form-check-input" type="checkbox" name="amenities[]"
                            value="Emergency Exit" />
                          <label for="Emergency-Exit" class="ms-2 form-check-label">Emergency Exit</label>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-4 col-md-3 p-2">
                        <div class="form-check">
                          <input id="Elevator" class="form-check-input" type="checkbox" name="amenities[]"
                            value="Elevator" />
                          <label for="Elevator" class="ms-2 form-check-label">Elevator</label>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-4 col-md-3 p-2">
                        <div class="form-check">
                          <input id="Garden" class="form-check-input" type="checkbox" name="amenities[]"
                            value="Garden" />
                          <label for="Garden" class="ms-2 form-check-label">Garden</label>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-4 col-md-3 p-2">
                        <div class="form-check">
                          <input id="Parking" class="form-check-input" type="checkbox" name="amenities[]"
                            value="Parking" />
                          <label for="Parking" class="ms-2 form-check-label">Parking</label>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-4 col-md-3 p-2">
                        <div class="form-check">
                          <input id="Balcony" class="form-check-input" type="checkbox" name="amenities[]"
                            value="Balcony" />
                          <label for="Balcony" class="ms-2 form-check-label">Balcony</label>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-4 col-md-3 p-2">
                        <div class="form-check">
                          <input id="Home-Theater" class="form-check-input" type="checkbox" name="amenities[]"
                            value="Home Theater" />
                          <label for="Home-Theater" class="ms-2 form-check-label">Home Theater</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    <!-- Banner Section End -->

    <!-- Featured Property Section End -->

    <section class="home-featured-property category-tab spacer">
      <div class="container">
        <h2 class="text-center" style="margin-bottom:50px;">Propiedades</h2>

        <div class="product-content">
          <!-- <div class="row justify-content-center mb-4">
              <div class="col-md-8">
                <div class="product-tab">
                  <ul
                    class="list-unstyled text-center font-semibold font-13 mt-5">
                    <li class="list-inline-item tab-item mb-3">
                      <a
                        class="tab-item px-2 py-1 rounded font-14 text-decoration-none active"
                        data-filter=".all"
                        href="#"
                        >All</a
                      >
                    </li>
                    <li class="list-inline-item tab-item mb-3">
                      <a
                        class="tab-item px-2 py-1 rounded font-14 text-decoration-none"
                        data-filter=".rentals"
                        href="#"
                        >Rentals</a
                      >
                    </li>
                    <li class="list-inline-item tab-item mb-3">
                      <a
                        class="tab-item px-2 py-1 rounded font-14 text-decoration-none"
                        data-filter=".sales"
                        href="#"
                        >Sales</a
                      >
                    </li>
                    <li class="list-inline-item tab-item mb-3">
                      <a
                        class="tab-item px-2 py-1 rounded font-14 text-decoration-none"
                        data-filter=".apartments"
                        href="#"
                        >Apartments</a
                      >
                    </li>
                    <li class="list-inline-item tab-item mb-3">
                      <a
                        class="tab-item px-2 py-1 rounded font-14 text-decoration-none"
                        data-filter=".houses"
                        href="#"
                        >Houses</a
                      >
                    </li>
                  </ul>
                </div>
              </div>
            </div> -->









          <div class="content">
            <div class="product-item">
              <div class="row">

                <?php
                // Check if the form is submitted
                if (isset($_POST['search'])) {

                  $reference = $_POST['comprar_ref'];
                  $ubicacion = $_POST['ubicacion'];
                  $tipo = $_POST['tipo'];
                  // Get the XML data
                  $xml = file_get_contents('http://procesos.apinmo.com/xml/v2/Ru7VkGYC/10143-web.xml');
                  // Create a SimpleXMLElement object
                  $xmlObj = simplexml_load_string($xml);

                  // Iterate through each propiedad
                  foreach ($xmlObj->propiedad as $propiedad) {
                    $id = (string) $propiedad->id;
                    $ref = (string) $propiedad->ref;
                    $titulo1 = (string) $propiedad->titulo1;
                    $foto1 = (string) $propiedad->foto1;
                    $accion = (string) $propiedad->accion;
                    $ciudad = (string) $propiedad->ciudad;
                    $precioinmo = (string) $propiedad->precioinmo;
                    $tipo_oferta = (string) $propiedad->tipo_ofer;
                    $banyos = (string) $propiedad->banyos;
                    $habitaciones = (string) $propiedad->habitaciones;
                    $habdobles = (string) $propiedad->habdobles;
                    $m_cons = (string) $propiedad->m_cons;

                    // Check if the current propiedad matches the search parameters
                    if (

                      ($ref == $reference || $reference == "") && ($ubicacion == $ciudad || $ubicacion == "") && ($tipo == $tipo_oferta || $tipo == "")
                    ) {
                      // Display matching data in a table row
                      ?>

                      <div class="col-md-6 col-lg-4 product-desc all rentals mb-4">
                        <div class="card">
                          <div class="featured-product-img position-relative">
                            <a href="propiedad.php?id=<?= $id ?>"><img src="<?= $foto1 ?>" alt="latest-property"
                                class="img-fluid w-100 latest-pro-img" /></a>
                            <div class="property-details mb-2">
                              <h4 class="text-white mb-0">
                                <?= $precioinmo ?><span>€</span>
                              </h4>
                              <div class="property-view ms-auto">
                                <a href="#"><i class="fa-regular fa-images"></i></a>
                                <a href="#"><i class="fa-solid fa-video"></i></a>
                              </div>
                            </div>
                            <div class="property-tag">
                              <ul class="list-unstyled mb-0">
                                <li class="bg-blue">
                                  <?= $accion ?>
                                </li>
                              </ul>
                            </div>
                            <!-- <span class="featured-property me-2 text-white bg-green"><i
                            class="fas fa-star font-12"></i></span>
                        <span class="hot-property me-2 text-white bg-orange"><i class="fas fa-fire font-12"></i></span> -->
                          </div>
                          <div class="card-body">
                            <p class="text-orange mb-0">
                              <?= $tipo_oferta ?>
                            </p>
                            <h4 class="font-20 product-title">
                              <a href="page-property-single-v1.html" class="text-decoration-none">
                                <?= $titulo1 ?>
                              </a>
                            </h4>
                            <p>
                              <i class="fas fa-map-marker-alt"></i>
                              <?= $ciudad ?>
                            </p>
                            <ul class="list-unstyled d-flex justify-content-between mb-0">
                              <li>
                                <p class="mb-0">
                                  <?= $habdobles ?> <i class="fas fa-bed"></i>
                                </p>
                                Habitaciones
                              </li>

                              <li>
                                <p class="mb-0">
                                  <?= $banyos ?> <i class="fas fa-sink"></i>
                                </p>
                                Baños
                              </li>

                              <li>
                                <p class="mb-0">
                                  <?= $m_cons ?> <i class="fas fa-vector-square"></i>
                                </p>
                                m2
                              </li>
                            </ul>
                          </div>
                          <div class="property-footer border-top">
                            <div class="card-body">
                              <div class="property-footer-meta d-flex align-items-center">


                              </div>
                            </div>
                          </div>
                        </div>
                      </div>








                      <?php
                    }
                  }
                } else {

                  $url = 'http://procesos.apinmo.com/xml/v2/Ru7VkGYC/10143-web.xml ';
                  $xml = file_get_contents($url);
                  $data = simplexml_load_string($xml);

                  foreach ($data->propiedad as $propiedad) {
                    $id = (string) $propiedad->id;
                    $titulo1 = (string) $propiedad->titulo1;
                    $foto1 = (string) $propiedad->foto1;
                    $accion = (string) $propiedad->accion;
                    $ciudad = (string) $propiedad->ciudad;
                    $precioinmo = (string) $propiedad->precioinmo;
                    $tipo_oferta = (string) $propiedad->tipo_ofer;
                    $banyos = (string) $propiedad->banyos;
                    $habitaciones = (string) $propiedad->habitaciones;
                    $habdobles = (string) $propiedad->habdobles;
                    $m_cons = (string) $propiedad->m_cons;




                    ?>








                    <div class="col-md-6 col-lg-4 product-desc all rentals mb-4">
                      <div class="card">
                        <div class="featured-product-img position-relative" href="propiedad.php?id=<?= $id ?>">
                          <a href="propiedad.php?id=<?= $id ?>"><img src="<?= $foto1 ?>" alt="latest-property"
                              class="img-fluid w-100 latest-pro-img" /></a>
                          <div class="property-details mb-2">
                            <h4 class="text-white mb-0">
                              <?= $precioinmo ?><span>€</span>
                            </h4>
                            <div class="property-view ms-auto">
                              <a href="#"><i class="fa-regular fa-images"></i></a>
                              <a href="#"><i class="fa-solid fa-video"></i></a>
                            </div>
                          </div>
                          <div class="property-tag">
                            <ul class="list-unstyled mb-0">
                              <li class="bg-blue">
                                <?= $accion ?>
                              </li>
                            </ul>
                          </div>
                          <!-- <span class="featured-property me-2 text-white bg-green"><i
                            class="fas fa-star font-12"></i></span>
                        <span class="hot-property me-2 text-white bg-orange"><i class="fas fa-fire font-12"></i></span> -->
                        </div>
                        <div class="card-body">
                          <p class="text-orange mb-0">
                            <?= $tipo_oferta ?>
                          </p>
                          <h4 class="font-20 product-title">
                            <a href="page-property-single-v1.html" class="text-decoration-none">
                              <?= $titulo1 ?>
                            </a>
                          </h4>
                          <p>
                            <i class="fas fa-map-marker-alt"></i>
                            <?= $ciudad ?>
                          </p>
                          <ul class="list-unstyled d-flex justify-content-between mb-0">
                            <li>
                              <p class="mb-0">
                                <?= $habdobles ?> <i class="fas fa-bed"></i>
                              </p>
                              Habitaciones
                            </li>

                            <li>
                              <p class="mb-0">
                                <?= $banyos ?> <i class="fas fa-sink"></i>
                              </p>
                              Baños
                            </li>

                            <li>
                              <p class="mb-0">
                                <?= $m_cons ?> <i class="fas fa-vector-square"></i>
                              </p>
                              m2
                            </li>
                          </ul>
                        </div>
                        <div class="property-footer border-top">
                          <div class="card-body">
                            <div class="property-footer-meta d-flex align-items-center">


                            </div>
                          </div>
                        </div>
                      </div>
                    </div>





                    <?php
                  }
                }
                ?>



              </div>
            </div>
          </div>
        </div>
        <!-- <div class="text-center mt-4">
            <a href="javascript:void(0)" class="btn btn-warning view-all-btn"
              >View All Property</a
            >
          </div> -->
      </div>
    </section>





    <script>
      $(document).ready(function () {
        // Handle form submission
        $("#searchForm").submit(function (event) {
          event.preventDefault(); // Prevent page reload


          var reference = $("#comprar_ref").val().trim();

          // Clear previous results
          $("#resultsTable tbody").empty();

          // Make AJAX request to fetch XML data
          $.ajax({
            type: "GET",
            url: "http://procesos.apinmo.com/xml/v2/Ru7VkGYC/10143-web.xml",
            dataType: "xml",
            success: function (xml) {
              // Parse the XML data and filter based on search parameters
              $(xml).find("propiedad").each(function () {

                var currentReference = $(this).find("ref").text().trim();

                // Check if the current item matches the search parameters
                if (

                  (reference === "" || currentReference === reference)
                ) {
                  // Append matching data to the table
                  var row = $("<tr></tr>");
                  row.append("<td>" + currentReference + "</td>");

                  // Add more cells for other fields if needed

                  $("#resultsTable tbody").append(row);
                }
              });
            },
            error: function () {
              console.log("Error loading XML file.");
            }
          });
        });
      });
    </script>







    <!-- Required JS -->
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/libs/fontawesome/js/all.js"></script>
    <script src="assets/js/isotope.pkgd.min.js"></script>
    <script src="assets/js/custom.js"></script>

    <script>
      var TxtType = function (el, toRotate, period) {
        this.toRotate = toRotate;
        this.el = el;
        this.loopNum = 0;
        this.period = parseInt(period, 10) || 2000;
        this.txt = "";
        this.tick();
        this.isDeleting = false;
      };

      TxtType.prototype.tick = function () {
        var i = this.loopNum % this.toRotate.length;
        var fullTxt = this.toRotate[ i ];

        if (this.isDeleting) {
          this.txt = fullTxt.substring(0, this.txt.length - 1);
        } else {
          this.txt = fullTxt.substring(0, this.txt.length + 1);
        }

        this.el.innerHTML = '<span class="wrap">' + this.txt + "</span>";

        var that = this;
        var delta = 200 - Math.random() * 100;

        if (this.isDeleting) {
          delta /= 2;
        }

        if (!this.isDeleting && this.txt === fullTxt) {
          delta = this.period;
          this.isDeleting = true;
        } else if (this.isDeleting && this.txt === "") {
          this.isDeleting = false;
          this.loopNum++;
          delta = 500;
        }

        setTimeout(function () {
          that.tick();
        }, delta);
      };

      window.onload = function () {
        var elements = document.getElementsByClassName("typewrite");
        for (var i = 0; i < elements.length; i++) {
          var toRotate = elements[ i ].getAttribute("data-type");
          var period = elements[ i ].getAttribute("data-period");
          if (toRotate) {
            new TxtType(elements[ i ], JSON.parse(toRotate), period);
          }
        }

        // INJECT CSS

        var css = document.createElement("style");
        css.type = "text/css";
        css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
        document.body.appendChild(css);
      };
    </script>
</body>

<!-- Mirrored from code-wrapper.com/royal-homes/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 May 2023 09:26:31 GMT -->

</html>