<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from code-wrapper.com/royal-homes/page-property-single-v1.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 May 2023 09:26:42 GMT -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title></title>

    <!-- Fonts  -->
    <link rel="preconnect" href="https://fonts.gstatic.com/">
    <link href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@300;400;500;600;700&amp;display=swap"
        rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="assets/libs/fontawesome/css/all.css">
    <link rel="stylesheet" href="assets/css/animate.min.css">
    <link rel="stylesheet" href="assets/css/style.css">

</head>

<body>



    <?php
    $url = 'http://procesos.apinmo.com/xml/v2/Ru7VkGYC/10143-web.xml';
    $xml = file_get_contents($url);
    $data = simplexml_load_string($xml);





    if (!isset($_GET['id'])) {
        header("Location:index.php");
        exit();
    }

    $id = $_GET['id'];

    // Retrieve the specific element based on the ID
    $element = null;
    foreach ($data->propiedad as $propiedad) {
        if ((string) $propiedad->id === $id) {
            $element = $propiedad;
            break;
        }
    }

    if (!$element) {
        echo 'Element not found.';
        exit();
    }









    $numfotos = $element->numfotos;


    $titulo1 = (string) $element->titulo1;
    $foto1 = (string) $element->foto1;
    $accion = (string) $element->accion;
    $ciudad = (string) $element->ciudad;
    $precioinmo = (string) $element->precioinmo;
    $tipo_oferta = (string) $element->tipo_ofer;
    $banyos = (string) $element->banyos;
    $habitaciones = (string) $element->habitaciones;
    $habdobles = (string) $element->habdobles;
    $m_cons = (string) $element->m_cons;
    $descrip1 = (string) $element->descrip1;

    $descrip1 = str_replace('~', '<p>', $descrip1);
    $descrip1 = str_replace('~', '</p>', $descrip1);

    $agente = (string) $element->agente;
    $email_agente = (string) $element->email_agente;
    $prefijo_tlf_agente = (string) $element->prefijo_tlf_agente;
    $tlf_agente = (string) $element->tlf_agente;
    $tour = (string) $element->tour;


    ?>






    <div class="main-wrapper">


        <section class="property-listing-title">
            <div class="container">
                <div class="row align-items-baseline">
                    <div class="col-lg-7">
                        <div class="d-md-flex align-items-baseline property-title">
                            <div class="property-title">
                                <h1 class="font-30">
                                    <?= $titulo1 ?>
                                </h1>
                                <p class="mb-0">
                                    <i class="fa-solid fa-location-dot"></i> <?= $ciudad ?>

                                                                        </p>
                            </div>

                            <div class="property-tag ms-md-auto ms-lg-3">
                                <ul class="list-inline">
                                    <li class="bg-dark list-inline-item">
                                        <?= $accion ?>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="d-md-flex align-items-center property-amnt">
                            <h2 class="font-30">
                                <?= $precioinmo ?><span>€</span>
                            </h2>
                            <ul class="list-inline mt-3 mt-md-0 mb-0 ms-md-auto property-options ">
                                <li class="list-inline-item">
                                    <a href="#"><i class="fas fa-exchange-alt"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#"><i class="far fa-heart"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#"><i class="fas fa-share-alt"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="javascript:void(0)" onclick="window.print()"><i
                                            class="fas fa-print"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="property-img-gallery">
            <div class="owl-carousel owl-theme property-img-carousal mt-5">


                <?php for ($i = 1; $i <= $numfotos; $i++) {

                    $foto = "foto" . $i;
                    $foto = $element->$foto;
                    if (!isset($foto['eti'])) {
                        ?>




                        <div class="property-item">
                            <img src="<?= $foto ?>" alt="property-img" class="property-img" />
                        </div>
                    <?php }
                } ?>
            </div>
            <div class="property-slider_nav">
                <span class="property-prev">
                    <i class="fas fa-chevron-left"></i>
                </span>

                <span class="property-next ml-3">
                    <i class="fas fa-chevron-right"></i>
                </span>

            </div>
        </div>

        <section class="property-details-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-8">
                        <div class="property-description mb-5">
                            <div style="display:flex; align-items:center; margin-bottom:20px;">
                                <h4 class=" font-20">Descripcion</h4>
                                <a class=" " href="<?= $tour ?>" style="margin-left:20px;"><img
                                        src="assets/360-degrees (1).png" alt=""></a>
                            </div>
                            <ul class="list-inline property-desc">
                                <li class="list-inline-item">
                                    <?= $habdobles ?> Habitaciones
                                </li>
                                <li class="list-inline-item">
                                    <?= $banyos ?> Baños
                                </li>
                                <li class="list-inline-item">
                                    <?= $m_cons ?> m2
                                </li>
                            </ul>
                            <?= $descrip1 ?>

                        </div>

                        <div class="property-details mb-5">
                            <h4 class="border-bottom pb-3 mb-4 font-20">Características</h4>

                            <div class="row">
                                <div class="col-md-6 col-lg-6 ">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-6 mb-2">Tipo :</div>
                                        <div class="col-lg-6 col-md-6 col-6 mb-2 text-alter font-averta-bold"></div>

                                        <div class="col-lg-6 col-md-6 col-6 mb-2">Precio :</div>
                                        <div class="col-lg-6 col-md-6 col-6 mb-2 text-alter font-averta-bold"> precio
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-6 mb-2">Parcela :</div>
                                        <div class="col-lg-6 col-md-6 col-6 mb-2 text-alter font-averta-bold">m2
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-6 mb-2">Año :</div>
                                        <div class="col-lg-6 col-md-6 col-6 mb-2 text-alter font-averta-bold">2020</div>

                                        <div class="col-lg-6 col-md-6 col-6 mb-2">Property Type :</div>
                                        <div class="col-lg-6 col-md-6 col-6 mb-2 text-alter font-averta-bold">Family
                                            House</div>

                                    </div>
                                </div>

                                <div class="col-md-6 col-lg-6 ">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-6 mb-2">Para :</div>
                                        <div class="col-lg-6 col-md-6 col-6 mb-2 text-alter font-averta-bold">Vender
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-6 mb-2">Habitaciones :</div>
                                        <div class="col-lg-6 col-md-6 col-6 mb-2 text-alter font-averta-bold">4</div>

                                        <div class="col-lg-6 col-md-6 col-6 mb-2">Baños :</div>
                                        <div class="col-lg-6 col-md-6 col-6 mb-2 text-alter font-averta-bold">2</div>

                                        <div class="col-lg-6 col-md-6 col-6 mb-2">Garaje :</div>
                                        <div class="col-lg-6 col-md-6 col-6 mb-2 text-alter font-averta-bold">1</div>

                                        <div class="col-lg-6 col-md-6 col-6 mb-2">Metros construidos :</div>
                                        <div class="col-lg-6 col-md-6 col-6 mb-2 text-alter font-averta-bold">m2
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="property-amenities mb-5">
                            <h4 class="border-bottom pb-3 mb-4 font-20">Extras</h4>
                            <ul class="list-inline">
                                <li>Air Conditioning</li>
                                <li>Barbeque</li>
                                <li>Dryer</li>
                                <li>Gym</li>
                                <li>Laundry</li>
                                <li>Lawn</li>
                                <li>Microwave</li>
                                <li>Outdoor Shower</li>
                                <li>Refrigerator</li>
                                <li>Sauna</li>
                                <li>Swimming Pool</li>
                                <li>TV Cable</li>
                                <li>Washer</li>
                                <li>WiFi</li>
                                <li>Window Coverings</li>
                            </ul>
                        </div>

                        <!-- <div class="property-dimension mb-5">
                            <h4 class="border-bottom pb-3 mb-4 font-20">Room Dimensions</h4>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-6 mb-2">Master Bedroom :</div>
                                        <div class="col-lg-6 col-md-6 col-6 mb-2 text-alter font-averta-bold">10 X 10
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-6 mb-2">Bedroom 1 :</div>
                                        <div class="col-lg-6 col-md-6 col-6 mb-2 text-alter font-averta-bold">20 X 15
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-6 mb-2">Living Room :</div>
                                        <div class="col-lg-6 col-md-6 col-6 mb-2 text-alter font-averta-bold">10 X 15
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-6 mb-2">Dining Room :</div>
                                        <div class="col-lg-6 col-md-6 col-6 mb-2 text-alter font-averta-bold">10 X 15
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-6 mb-2">Bedroom 2 :</div>
                                        <div class="col-lg-6 col-md-6 col-6 mb-2 text-alter font-averta-bold">15 X 18
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-6 mb-2">Bedroom 3 :</div>
                                        <div class="col-lg-6 col-md-6 col-6 mb-2 text-alter font-averta-bold">20 X 20
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-6 mb-2">Kitchen :</div>
                                        <div class="col-lg-6 col-md-6 col-6 mb-2 text-alter font-averta-bold">15 X 15
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-6 mb-2">Bathroom :</div>
                                        <div class="col-lg-6 col-md-6 col-6 mb-2 text-alter font-averta-bold">10 X 08
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-6 mb-2">Gym :</div>
                                        <div class="col-lg-6 col-md-6 col-6 mb-2 text-alter font-averta-bold">10 X 10
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-6 mb-2">Swimming Pool :</div>
                                        <div class="col-lg-6 col-md-6 col-6 mb-2 text-alter font-averta-bold">20 X 50
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div> -->

                        <!-- <div class="property-documents mb-5">
                            <h4 class="border-bottom pb-3 mb-4 font-20">Property Attachments</h4>

                            <div class="property-document-meta d-md-flex">
                                <div class="document-icon-box d-flex align-items-center">
                                    <div class="document-icon">
                                        <i class="far fa-file-word font-30 text-orange"></i>
                                    </div>
                                    <div class="document-details ms-3">
                                        <h5 class="font-16"><i class="fas fa-download me-2 text-orange"></i> Demo Word
                                            Document</h5>
                                    </div>
                                </div>
                                <div class="document-icon-box d-flex align-items-center ms-md-5 mt-3 mt-md-0">
                                    <div class="document-icon">
                                        <i class="far fa-file-pdf font-30 text-orange"></i>
                                    </div>
                                    <div class="document-details ms-3">
                                        <h5 class="font-16"><i class="fas fa-download me-2 text-orange"></i> Demo pdf
                                            Document</h5>
                                    </div>
                                </div>
                            </div>

                        </div> -->

                        <div class="property-floors mb-5">
                            <h4 class="border-bottom pb-3 mb-4 font-20">Plano </h4>

                            <div class="accordion" id="accordionFloor">



                            <?php for ($i = 1; $i <= $numfotos; $i++) {

$foto = "foto" . $i;
$foto = $element->$foto;
if (isset($foto['eti'])) {
    ?>


                                <div class="accordion-item">
                                    <div class="accordion-header" id="headingOne">
                                        <div class="accordion-button" data-bs-toggle="collapse"
                                            data-bs-target="#collapseOne" aria-expanded="true"
                                            aria-controls="collapseOne" role="list">
                                            <ul class="list-inline mb-0">
                                                <!-- <li class="list-inline-item text-alter font-averta-bold">First Floor
                                                </li>
                                                <li class="list-inline-item font-averta-regular text-muted">Size: <span
                                                        class="text-alter font-averta-bold">1200 Sqft</span></li>
                                                <li class="list-inline-item font-averta-regular text-muted">Rooms: <span
                                                        class="text-alter font-averta-bold">600 Sqft</span></li>
                                                <li class="list-inline-item font-averta-regular text-muted">Baths: <span
                                                        class="text-alter font-averta-bold">600 Sqft</span></li>
                                                <li class="list-inline-item font-averta-regular text-muted">Price: <span
                                                        class="text-alter font-averta-bold">$15,000</span></li> -->
                                            </ul>
                                        </div>
                                    </div>
                                    <div id="collapseOne" class="accordion-collapse collapse show"
                                        aria-labelledby="headingOne" data-bs-parent="#accordionFloor">
                                        <div class="accordion-body">
                                            <img class="img-fluid" src="<?= $foto ?>"
                                                alt="">
                                        </div>
                                    </div>
                                </div>

                                <?php }} ?>
                              

                            </div>
                        </div>





                        <!-- <div class="related-property-section mb-5">
                            <h4 class="border-bottom pb-3 mb-4 font-20">Related Property</h4>

                            <div class="related-property">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="card related-property-card mb-4">
                                            <div class="related-property-img position-relative">
                                                <img src="assets/images/property-img/fe-property-img-9.jpg"
                                                    alt="featured-property" class="img-fluid w-100 related-pro-img" />
                                                <div class="property-details mb-2">
                                                    <h4 class="text-white mb-0">$13,000/<span>mo</span></h4>
                                                    <div class="property-view ms-auto">
                                                        <a href="#"><i class="fas fa-exchange-alt"></i></a>
                                                        <a href="#"><i class="far fa-heart"></i></a>
                                                    </div>
                                                </div>
                                                <div class="property-tag">
                                                    <ul class="list-unstyled mb-0">
                                                        <li class="bg-blue">For Rent</li>
                                                    </ul>
                                                </div>
                                                <span class="featured-property me-2 text-white bg-green"><i
                                                        class="fas fa-star font-12"></i></span>
                                                <span class="hot-property me-2 text-white bg-orange"><i
                                                        class="fas fa-fire font-12"></i></span>
                                            </div>
                                            <div class="card-body">
                                                <p class="text-orange mb-0">Apartment</p>
                                                <h4 class="font-20 product-title"><a href="page-property-single-v1.html"
                                                        class="text-decoration-none">Modern Apartments</a></h4>
                                                <p><i class="fas fa-map-marker-alt"></i> 1421 San Pedro St, Los Angeles,
                                                    CA 90015</p>
                                                <ul class="list-unstyled d-flex justify-content-between mb-0">
                                                    <li>
                                                        <p class="mb-0">3 <i class="fas fa-bed"></i></p>
                                                        Bedrooms
                                                    <li>
                                                        <p class="mb-0">2 <i class="fas fa-sink"></i></p>
                                                        Bathrooms
                                                    <li>
                                                        <p class="mb-0">1250 <i class="fas fa-vector-square"></i> </p>
                                                        square Ft
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="property-footer border-top">
                                                <div class="card-body">
                                                    <div class="property-footer-meta d-flex align-items-center">
                                                        <a href="#"><img src="assets/images/user-icon.png"
                                                                alt="user-image" class="img-fluid"></a>
                                                        <div class="ms-3">
                                                            <h6 class="mb-0"><a href="#"
                                                                    class="text-decoration-none">Johnathan Doe</a></h6>
                                                            <p class="font-14 mb-0">Estate Agents</p>
                                                        </div>
                                                        <div class="property-year ms-auto">
                                                            <p class="mb-0">1 day Ago</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="card related-property-card mb-4">
                                            <div class="related-property-img position-relative">
                                                <img src="assets/images/property-img/fe-property-img-2.jpg"
                                                    alt="featured-property" class="img-fluid w-100 related-pro-img" />
                                                <div class="property-details mb-2">
                                                    <h4 class="text-white mb-0">$13,000/<span>mo</span></h4>
                                                    <div class="property-view ms-auto">
                                                        <a href="#"><i class="fas fa-exchange-alt"></i></a>
                                                        <a href="#"><i class="far fa-heart"></i></a>
                                                    </div>
                                                </div>
                                                <div class="property-tag">
                                                    <ul class="list-unstyled mb-0">
                                                        <li class="bg-orange">For Sale</li>
                                                    </ul>
                                                </div>

                                                <span class="hot-property me-2 text-white bg-green"><i
                                                        class="fas fa-star font-12"></i></span>
                                            </div>
                                            <div class="card-body">
                                                <p class="text-orange mb-0">House</p>
                                                <h4 class="font-20 product-title"><a href="page-property-single-v2.html"
                                                        class="text-decoration-none">Luxury Family Home</a></h4>
                                                <p><i class="fas fa-map-marker-alt"></i> 1421 San Pedro St, Los Angeles,
                                                    CA 90015</p>
                                                <ul class="list-unstyled d-flex justify-content-between mb-0">
                                                    <li>
                                                        <p class="mb-0">3 <i class="fas fa-bed"></i></p>
                                                        Bedrooms
                                                    <li>
                                                        <p class="mb-0">2 <i class="fas fa-sink"></i></p>
                                                        Bathrooms
                                                    <li>
                                                        <p class="mb-0">1250 <i class="fas fa-vector-square"></i> </p>
                                                        square Ft
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="property-footer border-top">
                                                <div class="card-body">
                                                    <div class="property-footer-meta d-flex align-items-center">

                                                        <div class="ms-3">
                                                            <h6 class="mb-0"><a href="#"
                                                                    class="text-decoration-none"><?= $agente ?></a></h6>
                                                            <p class="font-14 mb-0">Estate Agents</p>
                                                        </div>
                                                        <div class="property-year ms-auto">
                                                            <p class="mb-0">1 day Ago</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>

                        </div> -->

                    </div>
                    <div class="col-md-12 col-lg-4">

                        <div class="sidebar-agent-form mb-4 pb-1">
                            <div class="card">
                                <div class="agent-profile-image">

                                    <h4 class="font-20 mt-3">
                                        <?= $agente ?>
                                    </h4>
                                    <p>Agente Inmobiliario</p>
                                </div>
                                <div class="card-body p-4 border-bottom pb-2">
                                    <div class="agent-contact-detail-sidebar">
                                        <p><i class="fas fa-phone-square-alt me-3 text-orange"></i>
                                            +
                                            <?= $prefijo_tlf_agente ?>
                                            <?= $tlf_agente ?>
                                        </p>
                                        <p><i class="fas fa-envelope-square me-3 text-orange"></i><a href="#"
                                                class="text-decoration-none"> <?= $email_agente ?></a></p>
                                    </div>
                                </div>
                                <div class="card-body p-4">
                                    <div class="agent-contact-form-sidebar">
                                        <form id="agent-form" name="agent-form" method="POST">
                                            <input type="text" id="fname" name="full_name"
                                                class="form-control mb-3 font-14" placeholder="Nombre" required="">
                                            <input type="text" id="pnumber" name="p_number"
                                                class="form-control mb-3 font-14" placeholder="Telefono" required="">
                                            <input type="email" id="emailid" name="email_address"
                                                class="form-control mb-3 font-14" placeholder="Email" required="">
                                            <textarea rows="5" name="message" class="form-control mb-3 font-14"
                                                placeholder="Mensaje" required=""></textarea>

                                            <input type="submit" name="request"
                                                class="btn btn-warning w-100 agent-contact-btn" value="Enviar">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </section>
        <!-- Footer -->

    </div>

    <!-- Required JS -->
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/libs/fontawesome/js/all.js"></script>
    <script src="assets/js/isotope.pkgd.min.js"></script>
    <script src="assets/js/custom.js"></script>
</body>

<!-- Mirrored from code-wrapper.com/royal-homes/page-property-single-v1.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 May 2023 09:26:51 GMT -->

</html>